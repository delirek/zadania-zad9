# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from .models import Blog, Post


# Główna strona
@view_config(context='.models.ZMIEN_MNIE', renderer='templates/list.pt')
def view_blog(context, request):
    """ Wyświetla listę n ostatnich wpisów """
    pass


@view_config(context='.models.ZMIEN_MNIE', renderer='templates/view.pt')
def view_post(context, request):
    """ Wyświetla szczegółowo dany wpis """
    pass


@view_config(name='add', context='.models.ZMIEN_MNIE')
def add_post(context, request):
    """ Dodaje nowy wpis """
    pass